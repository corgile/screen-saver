﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace DesktopClock;

public partial class MainWindowViewModel : ObservableObject
{
    [ObservableProperty] private DateTime _currentTime;

    [ObservableProperty] private SolidColorBrush _foreground = null!;

    [ObservableProperty] private WindowState _windowState = WindowState.Maximized;

    public ICommand CloseCommand { get; }

    public MainWindowViewModel(Action action)
    {
        SleepManager.PreventSleep();
        CloseCommand = new RelayCommand(() =>
        {
            SleepManager.PreventSleep(false);
            action.Invoke();
        });
#pragma warning disable CS4014
        UpdateTime();
#pragma warning restore CS4014
    }

    private async Task UpdateTime()
    {
        while (true)
        {
            await Task.Delay(100);
            CurrentTime = DateTime.Now;
            Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString($"#{CurrentTime:hhmmss}"));
        }
        // ReSharper disable once FunctionNeverReturns
    }

    [RelayCommand]
    private void SwitchFullscreen() =>
        WindowState = WindowState.Equals(WindowState.Normal) ? WindowState.Maximized : WindowState.Normal;
}

public static class SleepManager
{
    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

    [Flags]
    private enum EXECUTION_STATE : uint
    {
        ES_AWAYMODE_REQUIRED = 0x00000040,
        ES_CONTINUOUS = 0x80000000,
        ES_DISPLAY_REQUIRED = 0x00000002,
        ES_SYSTEM_REQUIRED = 0x00000001
    }

    public static void PreventSleep(bool keepDisplayOn = true) => SetThreadExecutionState(keepDisplayOn ?
        EXECUTION_STATE.ES_SYSTEM_REQUIRED | EXECUTION_STATE.ES_DISPLAY_REQUIRED :
        EXECUTION_STATE.ES_SYSTEM_REQUIRED);
}